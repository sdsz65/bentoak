import { endpointURLFull } from 'config';

// ---------------------------------------------------------------
// PreLogin
// ---------------------------------------------------------------

export const ROUTE_ADMIN_USER_LOGIN = 'admin-users/actions/login';
export const ROUTE_ADMIN_USER_LOGOUT = 'admin-users/actions/logout';
export const ROUTE_ADMIN_USER_LIST = 'admin-users/';

export const ROUTE_COMPANY_USER_LOGIN = 'company-users/actions/login';
export const ROUTE_COMPANY_USER_LOGOUT = 'company-users/actions/logout';
export const ROUTE_COMPANY_USER_LIST = 'company-users/';

// ---------------------------------------------------------------

// ---------------------------------------------------------------
// PostLogin
// ---------------------------------------------------------------

export const ROUTE_ADMIN_USER = 'admin-users';
export const ROUTE_BILLING_RATE = 'billing-rates';

export const ROUTE_COMPANY_USER = 'company-users';

export const ROUTE_CUSTOMER = 'customers';

export const ROUTE_PRODUCT_CATEGORY = 'product-categories';

export const ROUTE_PRODUCT = 'products';
export const ROUTE_PRODUCT_CHECK_BRAND_NAME = 'products/actions/check-brand-name';
export const ROUTE_PRODUCT_CHECK_BRAND_SKU = 'products/actions/check-brand-sku';
export const ROUTE_PRODUCT_ATTRIBUTES = 'products/actions/list/attributes';
export const ROUTE_PRODUCT_BRANDS = 'products/actions/list/brands';
export const ROUTE_PRODUCT_CATEGORIES = 'products/actions/list/categories';
export const ROUTE_PRODUCT_IMPORT = 'products/actions/import';
export const ROUTE_IMPORT = 'import-products';

export const ROUTE_LINE_ITEM = 'line-items';
export const ROUTE_INVOICE = 'invoices';
export const ROUTE_PACKAGE = 'packages';

export const ROUTE_COMPANY = 'companies';

export const ROUTE_JOB = 'jobs';
export const ROUTE_JOB_FILE = 'job-files';
export const ROUTE_JOB_ACTION = 'jobs/actions';

export const ROUTE_JOB_BOOKING = 'job-bookings';

export const ROUTE_JOB_SELECTION = 'job-selections';
export const ROUTE_JOB_SELECTION_SORTED = 'job-selections/actions/sorted';
export const ROUTE_JOB_SELECTION_OPTIONS = (jobSelectionId) =>
  `job-selections/${jobSelectionId}/actions/options`;

export const ROUTE_SELECTION_NOTE = 'job-notes';
export const ROUTE_GLOBAL_JOB_NOTE = 'global-job-notes';
export const ROUTE_JOB_VERSION = 'job-versions';
export const ROUTE_JOB_ESTIMATION = 'job-estimations';

export const ROUTE_SCHEME = 'schemes';
export const ROUTE_SCHEME_DETAILED = (schemeId) =>
  `schemes/${schemeId}/actions/detailed`;
export const ROUTE_SCHEME_ITEM = 'scheme-items';
export const ROUTE_SCHEME_ITEM_OPTIONS = (schemeItem) =>
  `scheme-items/${schemeItem}/actions/options`;

export const ROUTE_COMPANY_REPORT = 'company-reports';
export const ROUTE_COMPANY_REPORT_DETAILED = 'company-reports/actions/detailed';

export const ROUTE_COMPANY_REPORT_SECTION = 'company-report-sections';
export const ROUTE_COMPANY_REPORT_SECTION_ITEM = 'company-report-section-items';

export const ROUTE_BRAND = 'brands';
export const ROUTE_SHOWROOM = 'showrooms';

export const ROUTE_LOG = 'logs';

export const ROUTE_SUPPLIER = 'suppliers';

export const ROUTE_SETTINGS = 'settings';
export const ROUTE_READ_SETTINGS_IDENTIFIER = (module) =>
  `settings/actions/${module}/read`;
export const ROUTE_UPDATE_SETTINGS_IDENTIFIER = (module, identifier) =>
  `settings/actions/${module}/update/${identifier}`;

export const ROUTE_REPORT_PRODUCT_BEST_SELLERS =
  'reports/products/best-sellers';
export const ROUTE_REPORT_CUSTOMERS_ORDERS = 'reports/customers/orders';
export const ROUTE_REPORT_CUSTOMERS_BY_CITY = 'reports/customers/by-city';
export const ROUTE_REPORT_CUSTOMERS_ACTIVE_BY_CITY =
  'reports/customers/active/by-city';

export const ROUTE_DASHBOARD_READ = 'dashboard';
// ---------------------------------------------------------------

export const ROUTE_DOWNLOAD_IMAGE = (
  apiRoute,
  resourceFieldName,
  resourceValue,
  size,
  token
) => {
  let fileName = '';
  if (resourceValue) {
    const endIndex = resourceValue.lastIndexOf('.');
    if (apiRoute === 'products' || endIndex === -1) {
      fileName = resourceValue;
    } else {
      fileName = resourceValue.substr(0, endIndex);
    }

    fileName +=
      size === 'thumbnail'
        ? '_t'
        : size === 'large'
        ? '_l'
        : size === 'medium'
        ? '_m'
        : '';
    if (apiRoute !== 'products' && endIndex !== -1) {
      fileName +=
        '.' + resourceValue.substr(resourceValue.lastIndexOf('.') + 1, 5);
    }
  }
  return `${endpointURLFull}${apiRoute}/actions/read-resource/${resourceFieldName}/${fileName}?token=${token}`;
};

export const ROUTE_DOWNLOAD_IMAGE_CUSTOMER = (
  apiRoute,
  resourceFieldName,
  resourceValue,
  id
) =>
  `${apiRoute}/${id}/actions/read-resource/${resourceFieldName}/${resourceValue}`;

export const GENERATE_ROUTE = (route, params = []) =>
  `${route}${params.length ? `/${params.join('/')}` : ''}`;

export const ROUTE_SETTING_READ = '/settings/actions/read';

//MARKUP ROUTE----------------------------------------------------

export const ROUTE_MARKUP_GET_LIST = `${endpointURLFull}markups`;

export const ROUTE_MARKUP_IMAGE_URL = (markupId, imageFileName, token) =>
  `${endpointURLFull}markups/${markupId}/actions/image/${imageFileName}?token=${token}`;
//
export const ROUTE_MARKUP_GET_PAGE = (markupId) =>
  `${endpointURLFull}markups/${markupId}`;

export const ROUTE_MARKUP_CREATE_ANNOTATIONS = (markupId, pageId, token) =>
  `markups/${markupId}/actions/pages/${pageId}/annotations`;
export const ROUTE_MARKUP_GET_ANNOTATIONS = (markupId, pageId, annotationId) =>
  `markups/${markupId}/actions/pages/${pageId}/annotations/${annotationId}`;

export const ROUTE_MARKUP_UPDATE_ANNOTATIONS = (
  markupId,
  pageId,
  annotationId
) => `markups/${markupId}/actions/pages/${pageId}/annotations/${annotationId}`;

export const ROUTE_MARKUP_DELETE_ANNOTATIONS = (markupId, pageId) =>
  `markups/${markupId}/actions/pages/${pageId}/annotations`;

export const ROUTE_DOWNLOAD_MARKUP = (markupId) =>
  `markups/actions/create-pdf/${markupId}`;

export const ROUTE_UPDATE_MARKUP_PAGE = (markupId, pageId) =>
  `markups/${markupId}/actions/pages/${pageId}`;

export const ROUTE_UPDATE_MARKUP_PAGE_ANNOTATION = (markupId, pageId) =>
  `markups/${markupId}/actions/pages/${pageId}/annotation-color`;

export const ROUTE_GET_PAGE_ITEM = (markupId, pageId) =>
  `markups/${markupId}/actions/pages/${pageId}`;
